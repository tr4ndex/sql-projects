USE [Customer Shop]
SELECT * FROM Customers

1. Jaka jest srednia ocena klientow "Spending Score" ze wzgledu na plec?

SELECT Gender, AVG(Spending_Score) as AVG_SS FROM Customers GROUP BY Gender 

2. Ilu klientow jest w kazdym z zawodow?

SELECT Profession, COUNT(Profession) as Liczba_klient�w FROM Customers GROUP BY Profession

3. Jaki jest sredni roczny przychod klientow wedlug profesji 

SELECT Profession, AVG(Annual_Income) as AVG_Annual FROM Customers GROUP BY Profession

4. Jaki jest maksymalny "Spending Score" klientow z liczba w rodzinie 3+?

SELECT  MAX(Spending_Score) as Maks_SS FROM Customers WHERE Family_Size >=3

5. Ilu pracownikow ma przynajmniej 5 lat doswiadczenia?

SELECT COUNT(*) as liczba_pracownikow FROM Customers WHERE Work_Experience >=5

6. Jaki jest sredni wiek pracownikow w kazdej profesji?

SELECT Profession, AVG(AGE) as AVG_wiek FROM Customers GROUP BY Profession

7. Jaki jest rozklad klientow wedlug plci i rodziny?

SELECT Gender, Family_Size, COUNT(*) as Total_Customers
FROM Customers
GROUP BY Gender, Family_Size
ORDER BY Gender, Family_Size

8. Ilu jest klientow ponizej 25 roku zycia majacych "Spending Score" ponizej 65?

SELECT  COUNT(*) as liczba_klient�w FROM Customers WHERE Age < 25 AND Spending_Score <65 

9. Jaki jest sredni roczny przychod pracownikow miedzy 30. a 40. rokiem zycia?

SELECT AVG(Annual_Income) as avg_przychod FROM Customers WHERE Age BETWEEN 30 AND 40

10. Jaki jest procent mezczyzn ze "Spending Score" powyzej 75 i rocznym przychodem powyzej 100 tys.? 

WITH
aaa AS ( 
SELECT COUNT(*) as liczba_mezczyzn
FROM Customers
WHERE Gender = 'Male'
),
bbb AS (
SELECT COUNT(*) as liczba
FROM Customers
WHERE Gender = 'Male'
AND Spending_Score > 75
AND Annual_Income > 100000
)
SELECT bbb.liczba * 100.0 / aaa.liczba_mezczyzn as procent_mezczyzn
FROM aaa,bbb

11. Ile procent klientow wszystkich profesji pracuje jako Inzynier?

WITH 
aaa AS (
SELECT  COUNT(*) as inzynier
FROM Customers
WHERE Profession = 'Engineer' 
),
bbb AS (
SELECT COUNT(*) as allpracownicy
FROM Customers
)
SELECT aaa.inzynier * 100.0 / bbb.allpracownicy as procent_inzynierow
FROM 
aaa, bbb






