USE Dostawcy
SELECT * FROM Orders

1. Calkowity koszt wydany na jedzenie (jedzenie, oplaty serwisowe, koszty dostawy)

SELECT SUM(Food_cost + Supplement_cost + Service_Fee + Delivery_cost) as allcosts FROM Orders

2. Ile wydalem od poczatku 2023 roku do dnia dzisiejszego? (calkowity koszt)

SELECT SUM(Food_cost) + SUM(Supplement_cost) + SUM(Service_Fee) + SUM(Delivery_cost) as AllCosts FROM Orders
WHERE Date BETWEEN '2023-01-01' AND GETDATE()

3. Ktory numer zamowienia (Order_ID) ma najwiekszy koszt dodatkow i ktora to firma?

Select TOP 1 Order_ID, Company, SUM(Supplement_cost) as max_koszt_dodatkow FROM Orders
GROUP BY Order_ID, Company
ORDER BY max_koszt_dodatkow desc

4. Ktory dostawca przywiozl najwiecej zamowien z McDonalds i ile ich bylo?

SELECT TOP 1 Supplier, COUNT(*) as najwiecejzmaca FROM Orders
WHERE Company = 'McDonald''s'
GROUP BY Supplier
ORDER BY najwiecejzmaca DESC

5. Ile zamowien zostalo zlozonych miedzy 01.01.2022 a 01.07.2022?

SELECT COUNT(*) as liczba_zamowien FROM Orders
WHERE Date BETWEEN '2022-01-01' AND '2022-07-01'

6. TOP 3 restauracje z ktorych najwiecej zamowilem

SELECT TOP 3 Company, COUNT(*) as liczba_zamowien FROM Orders
GROUP BY Company
ORDER BY liczba_zamowien DESC

7. Sredni calkowity koszt zamowien z kazdej firmy

SELECT Company, AVG(Food_cost + Supplement_cost + Service_Fee + Delivery_cost) as avg_cost
FROM Orders
GROUP BY Company
ORDER BY avg_cost DESC

8. Ile wydalem na same oplaty serwisowe i koszty dostawy?

SELECT SUM(Service_Fee + Delivery_cost) as sum_sf_dc FROM Orders

9. O ile % wi�cej wydalem na samo jedzenie (food_cost) w 2021 i 2022 ni� dotychczas w 2023 roku?

SELECT 
    (SUM(CASE WHEN YEAR(Date) = 2021 THEN Food_cost ELSE 0 END) +
     SUM(CASE WHEN YEAR(Date) = 2022 THEN Food_cost ELSE 0 END)) /
    SUM(CASE WHEN YEAR(Date) = 2023 THEN Food_cost ELSE 0 END) * 100 - 100 AS wzrost_procentowy
FROM Orders
WHERE YEAR(Date) IN (2021, 2022, 2023)

10. O ile % wiecej kosztowalo mnie najdrozsze zamowienie od najtanszego zamowienia?

SELECT ROUND(((MAX(Food_cost) - MIN(Food_Cost)) / MIN(Food_cost)) * 100, 2) as oileprocent  FROM Orders

11. Kt�re zam�wienie mia�o najwy�szy koszt spo�r�d wszystkich zam�wie� dostarczonych przez Glovo i ile wynosi�a suma wszystkich koszt�w?

SELECT Order_ID, Company, Supplier, 
SUM(Food_cost + Supplement_cost + Service_Fee + Delivery_cost) as sumaa
FROM Orders
WHERE (Food_cost + Supplement_cost + Service_Fee + Delivery_cost) = (SELECT MAX((Food_cost + Supplement_cost + Service_Fee + Delivery_cost))
FROM Orders
WHERE Supplier = 'Glovo')
GROUP BY Order_ID,  Company, Supplier

12. Zamowienie, ktore w nazwie jedzenia (Food) ma liter� ''g''

SELECT Food FROM Orders
WHERE Food LIKE '%g%'

13. 17. pod wzgl�dem kosztu jedzenie (food_cost) zamowienie

SELECT TOP 1 Food_cost FROM 
(SELECT DISTINCT TOP 17 Food_cost FROM Orders
ORDER BY Food_cost DESC) as sss
ORDER BY food_cost ASC

14. Suma calkowitych kosztow zamowien od wszystkich dostawcow, przedstawiona w kazdej kolumnie osobno

SELECT 
SUM(CASE WHEN Supplier = 'Uber Eats' THEN Food_cost + Supplement_cost + Service_Fee + Delivery_cost ELSE 0 END) as suma_kosztow_zamowien_uber_eats,
SUM(CASE WHEN Supplier = 'Glovo' THEN Food_cost + Supplement_cost + Service_Fee + Delivery_cost ELSE 0 END) as suma_kosztow_zamowien_glovo,
SUM(CASE WHEN Supplier = 'Wolt' THEN Food_cost + Supplement_cost + Service_Fee + Delivery_cost ELSE 0 END) as suma_kosztow_zamowien_wolt,
SUM(CASE WHEN Supplier = 'Pyszne' THEN Food_cost + Supplement_cost + Service_Fee + Delivery_cost ELSE 0 END) as suma_kosztow_zamowien_pyszne
FROM Orders


15. Ktory dostawca ma najwyzszy sredni koszt na jedno zamowienie?

SELECT TOP 1 Supplier, AVG(Food_cost + Supplement_cost + Service_Fee + Delivery_cost) as sredni_najwyzszy
FROM Orders
GROUP BY Supplier
ORDER BY AVG(Food_cost + Supplement_cost + Service_Fee + Delivery_cost) DESC

16. Przedzialy cenowe 'food_cost' wraz z liczba wystapien w danym przedziale cenowym.

SELECT 
   CASE
      WHEN Food_cost >= 23.95 AND Food_cost <=25 THEN '20-25'
	  WHEN Food_cost >= 25.01 AND Food_cost <= 30 THEN '25.01-30'
	  WHEN Food_cost >= 30.01 AND Food_cost <= 35 THEN '30.01-35'
	  WHEN Food_cost >= 35.01 AND Food_cost <= 40 THEN '35.01-40'
	  WHEN Food_cost >= 40.01 AND Food_cost <= 45 THEN '40.01-45'
	  WHEN Food_cost >= 45.01 AND Food_cost <= 50 THEN '45.01-50'
	  ELSE '50+'
	  END as price_ranges_of_food,
	  COUNT(*) as liczba_wystapien
	  FROM Orders
	  GROUP BY 
	  CASE
      WHEN Food_cost >= 23.95 AND Food_cost <=25 THEN '20-25'
	  WHEN Food_cost >= 25.01 AND Food_cost <= 30 THEN '25.01-30'
	  WHEN Food_cost >= 30.01 AND Food_cost <= 35 THEN '30.01-35'
	  WHEN Food_cost >= 35.01 AND Food_cost <= 40 THEN '35.01-40'
	  WHEN Food_cost >= 40.01 AND Food_cost <= 45 THEN '40.01-45'
	  WHEN Food_cost >= 45.01 AND Food_cost <= 50 THEN '45.01-50'
	  ELSE '50+'
	  END
	  
	 