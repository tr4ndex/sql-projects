SELECT * FROM Best
USE Netflix2022

--- TOP 10 most rating movies 

SELECT TOP 10 TITLE, MAX(Number_of_votes) as maxvotes FROM Best
GROUP BY TITLE
ORDER BY MAX(Number_of_votes) DESC

--- Average duration from TOP 10 Best Scored movies

SELECT AVG(DURATION) AS avgsduration 
FROM Best 
WHERE TITLE IN (SELECT TOP 10 TITLE FROM Best ORDER BY SCORE DESC);

--- Ktore filmy maja SCORE wyzszy niz sredni SCORE wszystkich filmow? 

SELECT TITLE, CONCAT (SCORE, '>', (SELECT AVG(SCORE) FROM Best)) as score_vs_avgofallmovies
FROM Best
WHERE SCORE > (SELECT AVG(SCORE) FROM Best)

SELECT TITLE, SCORE FROM Best WHERE SCORE > (SELECT AVG(SCORE) FROM Best)

--- Ktore kraje wyprodukowaly filmy o lacznej dlugosci wiekszej niz 3000 minut?

SELECT COUNTRY 
FROM (
  SELECT COUNTRY, SUM(Duration) AS total_duration
  FROM Best 
  GROUP BY Country
)  as subquery
WHERE total_duration > 3000

--- Tytuly filmow, ktore nie zostaly wyprodukowane W USA, Indiach i Wielkiej Brytanii.

SELECT TITLE, COUNTRY FROM Best
WHERE COUNTRY NOT IN ('US', 'IN', 'GB')

--- Ktore filmy maja wiecej niz 50000 glosow i trwaja mniej niz 2 godziny

SELECT TITLE FROM Best
WHERE NUMBER_OF_VOTES > 50000 AND DURATION < 120

--- Kt�re kraje maj� co najmniej dwa filmy na li�cie, ale nie maj� film�w z gatunku fantasy

SELECT DISTINCT COUNTRY 
FROM Best
WHERE COUNTRY IN (
  SELECT COUNTRY
  FROM Best
  GROUP BY COUNTRY
  HAVING COUNT(*) >= 2
)
AND COUNTRY NOT IN (
  SELECT COUNTRY
  FROM Best
  WHERE MAIN_GENRE = 'fantasy'
)


--- Ile filmow dokumentalnych z USA zostalo wydanych w latach 2015-2020?

SELECT COUNT(*) no_of_movies FROM Best
WHERE MAIN_GENRE = 'drama' AND RELEASE_YEAR BETWEEN 2015 AND 2022

--- Ile filmow zostalo wydanych w danym kraju?

SELECT COUNTRY, COUNT(*) as no_of_mvs FROM Best
GROUP BY COUNTRY
ORDER BY no_of_mvs DESC

--- TOP 1 najwyzej oceniane ("score") filmy z kazdego gatunku

SELECT TITLE, MAIN_GENRE, SCORE
FROM (
  SELECT TITLE, MAIN_GENRE, SCORE,
    ROW_NUMBER() OVER (PARTITION BY MAIN_GENRE ORDER BY SCORE DESC) as row_num
  FROM Best
) as subq
WHERE row_num = 1
ORDER BY SCORE DESC

--- Ile wystepuje filmow w poszczegolnych przedzialach liczby glosow?

SELECT
  CASE 
      WHEN NUMBER_OF_VOTES >= 10000 AND NUMBER_OF_VOTES <= 300000 THEN '10k-300k'
	  WHEN NUMBER_OF_VOTES > 300000 AND NUMBER_OF_VOTES <= 600000 THEN '300k-600k'
	  WHEN NUMBER_OF_VOTES > 600000 AND NUMBER_OF_VOTES <= 900000 THEN '600k-900k'
	  WHEN NUMBER_OF_VOTES > 900000 AND NUMBER_OF_VOTES <= 1200000 THEN '900k-1.2kk'
	  WHEN NUMBER_OF_VOTES > 1200000 AND NUMBER_OF_VOTES <= 1500000 THEN '1,2kk-1,5kk'
	  WHEN NUMBER_OF_VOTES > 1500000 AND NUMBER_OF_VOTES <= 1800000 THEN '1,5kk-1,8kk'
	  WHEN NUMBER_OF_VOTES > 1800000 AND NUMBER_OF_VOTES <= 2100000 THEN '1,8kk-2,1kk'
	  ELSE '2,1kk+'
	  END as przedzialyglosow,
	  COUNT(*) as liczba_filmow
	  FROM Best
	  GROUP BY 
	  CASE 
      WHEN NUMBER_OF_VOTES >= 10000 AND NUMBER_OF_VOTES <= 300000 THEN '10k-300k'
	  WHEN NUMBER_OF_VOTES > 300000 AND NUMBER_OF_VOTES <= 600000 THEN '300k-600k'
	  WHEN NUMBER_OF_VOTES > 600000 AND NUMBER_OF_VOTES <= 900000 THEN '600k-900k'
	  WHEN NUMBER_OF_VOTES > 900000 AND NUMBER_OF_VOTES <= 1200000 THEN '900k-1.2kk'
	  WHEN NUMBER_OF_VOTES > 1200000 AND NUMBER_OF_VOTES <= 1500000 THEN '1,2kk-1,5kk'
	  WHEN NUMBER_OF_VOTES > 1500000 AND NUMBER_OF_VOTES <= 1800000 THEN '1,5kk-1,8kk'
	  WHEN NUMBER_OF_VOTES > 1800000 AND NUMBER_OF_VOTES <= 2100000 THEN '1,8kk-2,1kk'
	  ELSE '2,1kk+'
	  END
	  ORDER BY liczba_filmow DESC

--- O ile wiecej glosow ma najczesciej oceniany film od najmniej ocenianego? 

SELECT MAX(NUMBER_OF_VOTES) - MIN(NUMBER_OF_VOTES) as difference FROM Best

--- Tytuly filmow trwajace dluzej niz 130 minut ze wszystkich gatunkow oprocz dramatu, komedii i thrillerow oraz wydanych po 2000 roku

SELECT TITLE, DURATION, MAIN_GENRE FROM Best
WHERE MAIN_GENRE NOT IN ('drama', 'comedy', 'thriller') 
AND DURATION > 130 
AND RELEASE_YEAR > 2000
